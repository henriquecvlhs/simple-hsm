/**
 * @file hsm.h
 *
 * @brief This header defines the public interface of a minimal framework for
 *        Hierarchical State Machines (HSM).
 *
 * Copyright (c) 2017 Henrique Carvalho Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __HSM_H__
#define __HSM_H__

/*****************************************************************************
 *
 * Constants and primary type definitions.
 *
 *****************************************************************************/

/**
 * @brief Constant defining a non existent trigger action in a transition.
 */
#define HSM_NO_ACTION NULL


/**
 * @brief Constant defining a non existent entry point for a state.
 */
#define HSM_NO_ENTRY NULL


/**
 * @brief Constant defining a non existent exit point for a state.
 */
#define HSM_NO_EXIT NULL


/**
 * @brief Macro for passing reference to states.
 *
 * @param[in] state    State whose reference is to be passed.
 */
#define HSM_STATE_REF(state) (hsm_state_t *) &(state)


/**
 * @brief Macro for passing reference to event data.
 *
 * @param[in] p_event_data    Pointer to event data whose reference is to be passed.
 */
#define HSM_EVENT_DATA_REF(p_event_data) (void *) (p_event_data)


/**
 * @brief Enum describing different types of hierarchical states.
 */
typedef enum hsm_state_type
{
    HSM_STATE_TYPE_LEAF,
    HSM_STATE_TYPE_FULL_LEAF,
    HSM_STATE_TYPE_PARENT,
    HSM_STATE_TYPE_FULL_PARENT,
} hsm_state_type_t;


/**
 * @brief Enum describing available support for transition history.
 */
typedef enum hsm_history
{
    HSM_HISTORY_SUPPORT,
    HSM_HISTORY_NO_SUPPORT,
} hsm_history_t;


/**
 * @brief Type for hierarchical state machine signals.
 *
 * @note Instead of using this type it is preferable to define
 *       an enum type to describe possible signals of each state machine.
 */
typedef uint8_t hsm_signal_t;


/**
 * @brief Type declaration for a hierarchical state machine (HSM).
 */
typedef struct hsm hsm_t;

/*****************************************************************************
 *
 * Types for HSM handlers.
 *
 *****************************************************************************/

/**
 * @brief Generic state entry point type for hierarchical state machine.
 *
 * @details All state entry point routines should follow this function pointer type.
 */
typedef void (* hsm_entry_t)(void);


/**
 * @brief Generic state exit point type for hierarchical state machine.
 *
 * @details All state exit point routines should follow this function pointer type.
 */
typedef void (* hsm_exit_t)(void);


/**
 * @brief Generic state handler type for hierarchical state machines.
 *
 * @param[in] p_hsm     Pointer to HSM instance.
 * @param[in] signal    Event signal to be handled by state machine.
 * @param[in] p_data    Pointer to event data.
 */
typedef void (* hsm_handler_t)(hsm_t *p_hsm, hsm_signal_t signal, void *p_data);


/**
 * @brief Generic state transition action handler type for hierarchical state machines.
 *
 * @details All transition action routines should implement a similar function, with their
 *          specific data type as argument.
 *
 * @param[in] p_data    Pointer to event data.
 */
typedef void (* hsm_action_t)(void *p_data);

/*****************************************************************************
 *
 * Types for HSM states.
 *
 *****************************************************************************/

/**
 * @brief Structure describing a minimal state in a hierarchical state machine.
 */
typedef struct hsm_state
{
    const hsm_state_type_t type;        /**< State type. */
    const hsm_handler_t    handler;     /**< Pointer to state handler routine. */
} hsm_state_t;


/**
 * @brief Structure describing a simple leaf state in a hierarchical state machine.
 */
typedef struct hsm_state_leaf
{
    struct hsm_state;                   /**< Inherit from HSM state type. */
} hsm_state_leaf_t;


/**
 * @brief Structure describing a complete leaf state in a hierarchical state machine.
 */
typedef struct hsm_state_full_leaf
{
    struct hsm_state_leaf;              /**< Inherit from simple leaf state. */
    const hsm_entry_t      entry;       /**< Pointer to state entry point routine. */
    const hsm_exit_t       exit;        /**< Pointer to state exit point routine. */
} hsm_state_full_leaf_t;


/**
 * @brief Structure describing a simple parent state in a hierarchical state machine.
 */
typedef struct hsm_state_parent
{
    struct hsm_state;                   /**< Inherit from HSM state type. */
    hsm_history_t      history;         /**< Transition history support. */
    hsm_state_t       *p_nested_state;  /**< Pointer to nested state. */
} hsm_state_parent_t;


/**
 * @brief Structure describing a full parent state in a hierarchical state machine.
 */
typedef struct hsm_state_full_parent
{
    struct hsm_state_parent;            /**< Inherit from simple parent state. */
    const hsm_entry_t        entry;     /**< Pointer to state entry point routine. */
    const hsm_exit_t         exit;      /**< Pointer to state exit point routine. */
} hsm_state_full_parent_t;

/*****************************************************************************
 *
 * Types for HSM instances and transitions.
 *
 *****************************************************************************/

/**
 * @brief Structure describing a hierarchical state machine.
 */
struct hsm
{
    uint8_t       stack_top;                /**< Points to the top of the stack. */
    uint8_t       stack_size;               /**< Maximum number of nested states. */
    hsm_state_t **p_hsm_stack;              /**< HSM stack of active states. */
};


/**
 * @brief Type for hierarchical sate machine transitions.
 */
typedef struct hsm_transition
{
    hsm_state_t  *p_current_state;          /**< Source state for transition. */
    hsm_state_t  *p_next_state;             /**< Target state for transition. */
    hsm_action_t  p_action;                 /**< Action to be performed as consequence of transition. */
    bool          b_is_valid;               /**< Boolean flag signaling if transition is valid. */
    bool          b_is_internal;            /**< Boolean flag signaling if transition is internal. */
} hsm_transition_t;

/*****************************************************************************
 *
 * Macros for defining HSM and HSM state instances.
 *
 *****************************************************************************/

/**
 * @brief Create leaf state for hierarchical state machine.
 *
 * @param[in] state_name      Name attributed to this state.
 */
#define HSM_STATE_CREATE_LEAF(state_name)                                   \
    static void state_name##_handler(hsm_t        *p_hsm,                   \
                                     hsm_signal_t  signal,                  \
                                     void         *p_data);                 \
    static hsm_state_leaf_t state_name =                                    \
    {                                                                       \
        .type = HSM_STATE_TYPE_LEAF,                                        \
        .handler = state_name##_handler,                                    \
    }                                                                       \


/**
 * @brief Create full leaf state for hierarchical state machine.
 *
 * @param[in] state_name      Name attributed to this state.
 * @param[in] state_entry     Handler for state entry routine.
 * @param[in] state_exit      Handler for state exit routine.
 */
#define HSM_STATE_CREATE_FULL_LEAF(state_name, state_entry, state_exit)     \
    static void state_name##_handler(hsm_t        *p_hsm,                   \
                                     hsm_signal_t  signal,                  \
                                     void         *p_data);                 \
    static hsm_state_full_leaf_t state_name =                               \
    {                                                                       \
        .type = HSM_STATE_TYPE_FULL_LEAF,                                   \
        .handler = state_name##_handler,                                    \
        .entry = (hsm_entry_t) state_entry,                                 \
        .exit = (hsm_exit_t) state_exit,                                    \
    }                                                                       \


/**
 * @brief Create parent state for hierarchical state machine.
 *
 * @param[in] state_name      Name attributed to this state.
 * @param[in] history_support Support for keeping transition history.
 * @param[in] p_nested_state  Pointer to initial state of nested state machine.
 */
#define HSM_STATE_CREATE_PARENT(state_name, history_support, nested_state)  \
    static void state_name##_handler(hsm_t        *p_hsm,                   \
                                     hsm_signal_t  signal,                  \
                                     void         *p_data);                 \
    static hsm_state_parent_t state_name =                                  \
    {                                                                       \
        .type = HSM_STATE_TYPE_PARENT,                                      \
        .handler = state_name##_handler,                                    \
        .history = history_support,                                         \
        .p_nested_state = nested_state,                                     \
    }                                                                       \


/**
 * @brief Create state for hierarchical state machine.
 *
 * @param[in] state_name      Name attributed to this state.
 * @param[in] history_support Support for keeping transition history.
 * @param[in] p_nested_state  Pointer to initial state of nested state machine.
 * @param[in] state_entry     Handler for state entry routine.
 * @param[in] state_exit      Handler for state exit routine.
 */
#define HSM_STATE_CREATE_FULL_PARENT(state_name, history_support,           \
                                     nested_state, state_entry, state_exit) \
    static void state_name##_handler(hsm_t        *p_hsm,                   \
                                     hsm_signal_t  signal,                  \
                                     void         *p_data);                 \
    static hsm_state_full_parent_t state_name =                             \
    {                                                                       \
        .type = HSM_STATE_TYPE_FULL_PARENT,                                 \
        .handler = state_name##_handler,                                    \
        .history = history_support,                                         \
        .p_nested_state = nested_state,                                     \
        .entry = (hsm_entry_t) state_entry,                                 \
        .exit = (hsm_exit_t) state_exit,                                    \
    }                                                                       \


/**
 * @brief Create a hierarchical state machine instance.
 *
 * @param[in] hsm_name             Name attributed to this hierarchical state machine.
 * @param[in] hsm_stack_size       Maximum number of hierarchical state levels.
 */
#define HSM_CREATE(hsm_name, hsm_stack_size)                                \
    static hsm_state_t *hsm_name##_stack[hsm_stack_size + 1] = { NULL };    \
    static hsm_t hsm_name =                                                 \
    {                                                                       \
        .stack_top = 0,                                                     \
        .stack_size = hsm_stack_size + 1,                                   \
        .p_hsm_stack = hsm_name##_stack,                                    \
    };                                                                      \

/*****************************************************************************
 *
 * Macros for defining HSM transitions.
 *
 *****************************************************************************/

/**
 * @brief Create a relationship between all non specified signals and an array of
 *        possible state machine transitions, thus creating a "default" state
 *        transition.
 *
 * @details This macro must be used along with macro HSM_STATE_TRANSITION_TABLE
 *          and the following macros in order to declare the possible transitions
 *          for an event:
 *              @ref HSM_GUARD_TRANSITION,
 *              @ref HSM_STATE_TRANSITION,
 *              @ref HSM_SELF_TRANSITION,
 *              @ref HSM_INTERNAL_TRANSITION.
 *
 * @param[in] signal_id             Enum constant related to event signal.
 * @param[in] array_of_transitions  Array of at least one transition specified
 *                                  using the macros detailed above.
 *
 * @note It will only make sense to put more than one transition in array_of_transitions
 *       if these are declared with the HSM_GUARD_TRANSITION macro.
 */
#define HSM_TRANSITION_ON_ANY_SIGNAL(array_of_transitions)                  \
    default:                                                                \
        array_of_transitions                                                \
    break                                                                   \


/**
 * @brief Create a relationship between an event signal and an array of
 *        possible state machine transitions.
 *
 * @details This macro must be used along with macro HSM_STATE_TRANSITION_TABLE
 *          and the following macros in order to declare the possible transitions
 *          for an event:
 *              @ref HSM_GUARD_TRANSITION,
 *              @ref HSM_STATE_TRANSITION,
 *              @ref HSM_SELF_TRANSITION,
 *              @ref HSM_INTERNAL_TRANSITION.
 *
 * @param[in] signal_id             Enum constant related to event signal.
 * @param[in] array_of_transitions  Array of at least one transition specified
 *                                  using the macros detailed above.
 *
 * @note It will only make sense to put more than one transition in array_of_transitions
 *       if these are declared with the HSM_GUARD_TRANSITION macro.
 */
#define HSM_TRANSITION_ON_SIGNAL(signal_id, array_of_transitions)           \
    case (signal_id):                                                       \
        array_of_transitions                                                \
    break                                                                   \


/**
 * @brief Create a guard condition for a state machine transition.
 *
 * @details This macro must be used with @ref HSM_TRANSITION_ON_SIGNAL to create a state
 *          machine transition for an event in the context of a state transition table.
 *
 * @param[in] guard_clause     Boolean condition that will trigger a transition in case it
 *                             is evaluated to true.
 * @param[in] hsm_transition   Transition defines with one of the following macros:
 *                                  @ref HSM_STATE_TRANSITION,
 *                                  @ref HSM_SELF_TRANSITION,
 *                                  @ref HSM_INTERNAL_TRANSITION.
 */ 
#define HSM_GUARD_TRANSITION(guard_clause, hsm_transition)                  \
    if (guard_clause)                                                       \
    {                                                                       \
        hsm_transition                                                      \
    }                                                                       \


/**
 * @brief Create state transition, a relationship between an event and a state that causes
 *        a transition to a next state, often triggering an action.
 *
 * @note State transitions must be a relation between two states in the same state machine.
 *       This means there cannot be a transition from a state to a sub state, or vice-versa.
 *       Both states involved in the relationship must always be in the same hierarchy level.
 *
 * @param[in] next_state      Reference to next state assumed by HSM after transition
 *                            (use @ref HSM_STATE_REF macro).
 * @param[in] action          Action triggered with state transition. Function pointer of type
 *                            @ref hsm_action_t. Use macro @ref HSM_NO_ACTION in case no action
 *                            is to be triggered.
 */ 


#define HSM_STATE_TRANSITION(next_state_ref, action)                        \
    transition.b_is_valid = true;                                           \
    transition.p_action = (hsm_action_t) (action);                          \
    transition.p_next_state = (next_state_ref);                             \


/**
 * @brief Create state self transition, from a state to itself. This can trigger
 *        an action.
 *
 * @note Self transitions, unlike internal transitions, will cause exiting and entering
 *       the same state (and its sub states).
 *
 * @param[in] action          Action triggered with state transition. Function pointer of type
 *                            @ref hsm_action_t. Use macro @ref HSM_NO_ACTION in case no action
 *                            is to be triggered.
 */ 
#define HSM_SELF_TRANSITION(action)                                         \
    transition.b_is_valid = true;                                           \
    transition.p_action = (hsm_action_t) (action);                          \


/**
 * @brief Create state internal transition, from a state to itself. This can trigger
 *        an action.
 *
 * @note Internal transitions, unlike self transitions, will not cause exiting and entering
 *       the same state (and its sub states).
 *
 * @param[in] action          Action triggered with state transition. Function pointer of type
 *                            @ref hsm_action_t. Use macro @ref HSM_NO_ACTION in case no action
 *                            is to be triggered.
 */ 
#define HSM_INTERNAL_TRANSITION(action)                                     \
    transition.b_is_valid = true;                                           \
    transition.b_is_internal = true;                                        \
    transition.p_action = (hsm_action_t) (action);                          \


/**
 * @brief Create state transition table for a given state.
 *
 * @details Each state must have a transition table. It must be created using this macro
 *          with aid of transition macros above.
 *
 * @param[in] state_name     Name of state related to this transition table.
 * @param[in] table_entries  List of state transitions declared using macro
 *                           @ref HSM_TRANSITION_ON_SIGNAL.
 *                           Declare them like that:
 *                              { HSM_TRANSITION_ON_SIGNAL(...);  ... }
 */
#define HSM_STATE_TRANSITION_TABLE(state_name, table_entries)               \
    static void state_name##_handler(hsm_t        *p_hsm,                   \
                                     hsm_signal_t  signal,                  \
                                     void         *p_data)                  \
    {                                                                       \
        hsm_transition_t transition =                                       \
        {                                                                   \
            .p_current_state = HSM_STATE_REF(state_name),                   \
            .p_next_state = HSM_STATE_REF(state_name),                      \
            .b_is_internal = false,                                         \
            .b_is_valid = false,                                            \
            .p_action = NULL,                                               \
        };                                                                  \
                                                                            \
        switch (signal)                                                     \
            table_entries                                                   \
                                                                            \
        hsm_transition_handle(p_hsm, &transition, p_data);                  \
    }                                                                       \

/*****************************************************************************
 *
 * Public HSM functions.
 *
 *****************************************************************************/

/**
 * @brief Generic HSM event handler.
 *
 * @details This should be called by application to handle events using a HSM 
 *          instance.
 *
 * @param[in] p_hsm     Pointer to HSM instance to handle given event.
 * @param[in] signal    Event signal.
 * @param[in] p_data    Pointer to event data. Use macro @ref HSM_EVENT_DATA_REF.
 */
void hsm_event_handle(hsm_t *p_hsm, hsm_signal_t signal, void *p_data);


/**
 * @brief Handle HSM transition.
 *
 * @details This should not be directly called by application. In other words,
 *          it is an exposed private function.
 *
 * @param[in] p_hsm            Pointer to HSM instance to handle given event.
 * @param[in] p_transmition    Pointer to state transition handler.
 * @param[in] p_data           Pointer to event data.
 */
void hsm_transition_handle(hsm_t            *p_hsm,
                           hsm_transition_t *p_transition,
                           void             *p_data);


/**
 * @brief Initialize HSM instance.
 *
 * @details This should be called by application to initialize a HSM instance.
 *
 * @param[in] p_hsm              Pointer to HSM instance to be initialized.
 * @param[in] p_initial_state    Reference to initial state for this HSM (use macro
 *                               @ref HSM_STATE_REF).
 */
void hsm_init(hsm_t *p_hsm, hsm_state_t *p_initial_state);

#endif /** __HSM_H__ */
