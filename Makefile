
test: hsm_test
	./hsm_test

watch: hsm_watch
	./hsm_watch

hsm_test: hsm_test.o hsm.o
	gcc -std=c99 -o hsm_test hsm_test.o hsm.o

hsm_watch: hsm_watch.o hsm.o
	gcc -std=c99 -o hsm_watch hsm_watch.o hsm.o -lncurses

hsm.o: hsm.c hsm.h
	gcc -std=c99 hsm.c -o hsm.o -c -fms-extensions -Wall -Werror

hsm_test.o: hsm_test.c hsm.h
	gcc -std=c99 hsm_test.c -o hsm_test.o -c -fms-extensions -Wall -Werror

hsm_watch.o: hsm_watch.c hsm.h
	gcc -std=c99 hsm_watch.c -o hsm_watch.o -c -fms-extensions -Wall -Werror

clean:
	rm *.o hsm_watch hsm_test
