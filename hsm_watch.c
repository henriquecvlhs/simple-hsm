/**
 * @file hsm_watch.c
 *
 * @brief Example of a simple watch implemented using HSM.
 *
 * Copyright (c) 2017 Henrique Carvalho Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <ncurses.h>
#include <stdint.h>
#include <stdbool.h>

#include "hsm.h"

typedef enum keypress
{
    KEYPRESS_NONE,
    KEYPRESS_SET,
    KEYPRESS_MODE,
    KEYPRESS_RESET,
} keypress_t;

typedef struct time
{
    uint8_t hour;
    uint8_t minute;
    uint8_t day;
    uint8_t month;
} time_t;


time_t g_current_time;
bool gb_is_time_set;

static void reset_watch(void)
{
    printf("\r\nWatch Example - Press:\r\nm == MODE, s == SET, r == RESET, q == QUIT.\r\n");

    g_current_time.hour = 0;
    g_current_time.minute = 0;
    g_current_time.day = 1;
    g_current_time.month = 1;
}

static void display_time(void)
{
    printf("%2.2d : %2.2d\r\n", g_current_time.hour, g_current_time.minute);
}

static void display_date(void)
{
    printf("%2.2d / %2.2d\r\n", g_current_time.month, g_current_time.day);
}

static void enter_setting_mode(void)
{
    gb_is_time_set = false;
    printf("Setting mode.\r\n");
    display_time();
}

static void mark_time_as_set(void)
{
    gb_is_time_set = true;
    printf("Time is set!\r\nTimekeeping mode.\r\n");
}

static void increase_hour(void)
{
    g_current_time.hour = (g_current_time.hour + 1) % 24;
    display_time();
}

static void increase_minute(void)
{
    g_current_time.minute = (g_current_time.minute + 1) % 60;
    display_time();
}

static void increase_day(void)
{
    g_current_time.day = (g_current_time.day + 1) % 31;
    g_current_time.day = (g_current_time.day == 0) ? 1 : g_current_time.day;
    display_date();
}

static void increase_month(void)
{
    g_current_time.month = (g_current_time.month + 1) % 13;
    g_current_time.month = (g_current_time.month == 0) ? 1 : g_current_time.month;
    display_date();
}

/** State machine nested inside Timekeeping mode state. */
HSM_STATE_CREATE_LEAF(time_state);
HSM_STATE_CREATE_LEAF(date_state);

/** State machine nested inside Setting mode state. */
HSM_STATE_CREATE_LEAF(hour_state);
HSM_STATE_CREATE_LEAF(minute_state);
HSM_STATE_CREATE_LEAF(day_state);
HSM_STATE_CREATE_LEAF(month_state);

/** Mode state machine. */
HSM_STATE_CREATE_PARENT(timekeeping_state, HSM_HISTORY_NO_SUPPORT, HSM_STATE_REF(time_state));
HSM_STATE_CREATE_FULL_PARENT(setting_state, HSM_HISTORY_NO_SUPPORT, HSM_STATE_REF(hour_state), enter_setting_mode, HSM_NO_EXIT);

/** Main state machine. */
HSM_STATE_CREATE_FULL_PARENT(watch_on_state, HSM_HISTORY_NO_SUPPORT, HSM_STATE_REF(setting_state), reset_watch, HSM_NO_EXIT);

/** Transition table for Time state. */
HSM_STATE_TRANSITION_TABLE(time_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_MODE,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(date_state), display_date)
        });
})

/** Transition table for Date state. */
HSM_STATE_TRANSITION_TABLE(date_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_MODE,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(time_state), display_time)
        });
})

/** Transition table for Hour state. */
HSM_STATE_TRANSITION_TABLE(hour_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_SET,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(minute_state), HSM_NO_ACTION)
        });
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_MODE,
        {
            HSM_SELF_TRANSITION(increase_hour)
        });
})

/** Transition table for Minute state. */
HSM_STATE_TRANSITION_TABLE(minute_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_SET,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(day_state), display_date)
        });
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_MODE,
        {
            HSM_SELF_TRANSITION(increase_minute)
        });
})

/** Transition table for Day state. */
HSM_STATE_TRANSITION_TABLE(day_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_SET,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(month_state), HSM_NO_ACTION)
        });
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_MODE,
        {
            HSM_SELF_TRANSITION(increase_day)
        });
})

/** Transition table for Day state. */
HSM_STATE_TRANSITION_TABLE(month_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_SET,
        {
            HSM_INTERNAL_TRANSITION(mark_time_as_set)
        });
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_MODE,
        {
            HSM_SELF_TRANSITION(increase_month)
        });
})

/** Transition table for Timekeeping state. */
HSM_STATE_TRANSITION_TABLE(timekeeping_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_SET,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(setting_state), HSM_NO_ACTION)
        });
})

/** Transition table for Setting state. */
HSM_STATE_TRANSITION_TABLE(setting_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_SET,
        {
            HSM_GUARD_TRANSITION(gb_is_time_set,
                                 HSM_STATE_TRANSITION(HSM_STATE_REF(timekeeping_state), display_time))
            HSM_GUARD_TRANSITION(!gb_is_time_set,
                                 HSM_INTERNAL_TRANSITION(HSM_NO_ACTION))
        });
})

/** Transition table for Watch On state. */
HSM_STATE_TRANSITION_TABLE(watch_on_state,
{
    HSM_TRANSITION_ON_SIGNAL(KEYPRESS_RESET,
        {
            HSM_SELF_TRANSITION(HSM_NO_ACTION)
        });
})


HSM_CREATE(watch_hsm, 3);


static void ncurses_init(void)
{
    WINDOW *w;

    w = initscr();      /* Initialize ncurses library. */
    raw();              /* Terminal in raw mode, no buffering. */
    noecho();           /* Disable echoing. */
    nonl();             /* Disable newline/return mapping. */
    timeout(250);       /* Timeout for getch function (250 ms). */
    keypad(w, FALSE);   /* FALSE: CSI codes, TRUE: curses codes for keys. */
}


int main (void)
{
    ncurses_init();

    bool b_is_initialized = false;

    while (1)
    {
        char c = getch();

        /** Initialize here because of ncurses. */
        if (!b_is_initialized)
        {
            hsm_init(&watch_hsm, HSM_STATE_REF(watch_on_state));
            b_is_initialized = true;
        }

        keypress_t keypress = KEYPRESS_NONE;

        if (c == 'm')
        {
            keypress = KEYPRESS_MODE;
        }
        else if (c == 's')
        {
            keypress = KEYPRESS_SET;
        }
        else if (c == 'r')
        {
            keypress = KEYPRESS_RESET;
        }
        else if (c == 'q')
        {
            break;
        }
        else
        {
            continue;
        }

        hsm_event_handle(&watch_hsm, keypress, NULL);
    }

    endwin();

    return 0;
}
