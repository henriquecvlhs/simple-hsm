# Simple HSM Implementation

This is a simple Hierarchical State Machine implementation for the C Programming Language with a focus on emdedded systems, aiming for low memory footprint.

Current implementation does not offer support for orthogonal state mechanism. Also, state transitions can only be defined as a relationship between two states in the same hierarchy level. The following features are supported:

* Nested states;
* Entry and exit routines for each state;
* Internal transitions, self transitions, guarded transitions;
* Optional state transition history.

Files hsm_test.c and hsm_watch.c are examples of how to use HSMs using this implementation. File hsm_test.c will only make use of implemented features in order to process a series of events in a predefined order. File hsm_watch.c implements a very simple watch with setting mode and timekeeping mode. Both examples can be built and run using the provided Makefile with commands _make test_ and _make watch_.
