/**
 * @file hsm.h
 *
 * @brief This file implements a minimal framework for Hierarchical State 
 *        Machines (HSM).
 *
 * Copyright (c) 2017 Henrique Carvalho Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "hsm.h"

/*****************************************************************************
 *
 * Functions and macros for manipulating HSM data structure.
 *
 *****************************************************************************/

/**
 * @brief Macro for peeking HSM nested states in hierarchical order.
 *
 * @details Put this inside a for statement.
 *
 * @param[in] p_state Name for HSM State iterator.
 * @param[in] p_hsm   Pointer to HSM instance.
 */
#define EACH_STATE_IN_HSM_NESTED_STATES(p_state, p_hsm)                                             \
    hsm_state_t **pp_state = &(p_hsm->p_hsm_stack[p_hsm->stack_top]), *p_state = *pp_state; \
    pp_state > &(p_hsm->p_hsm_stack[0]);                                                            \
    pp_state -= 1, p_state = *pp_state                                                              \


/**
 * @brief Insert state to lowest HSM hierarchy.
 *
 * @param[in] p_hsm   Pointer to HSM instance.
 * @param[in] p_state Pointer to state to be pushed into HSM hierarchy.
 */
static inline bool hsm_push_state(hsm_t *p_hsm, hsm_state_t *p_state)
{
    bool b_is_successful = false;

    if (p_hsm->stack_top < p_hsm->stack_size - 1)
    {
        p_hsm->stack_top++;
        p_hsm->p_hsm_stack[p_hsm->stack_top] = p_state;
        b_is_successful = true;
    }

    return b_is_successful;
}


/**
 * @brief Extract state from lowest HSM hierarchy.
 *
 * @param[in] p_hsm   Pointer to HSM instance.
 *
 * @return p_state Pointer to state to be pushed into HSM hierarchy.
 * @retval NULL If there is no state to be poped.
 */
static inline hsm_state_t *hsm_pop_state(hsm_t *p_hsm)
{
    hsm_state_t *p_state;

    if (p_hsm->stack_top > 0)
    {
        p_state = p_hsm->p_hsm_stack[p_hsm->stack_top];
        p_hsm->stack_top -= 1;
    }
    else
    {
        p_state = NULL;
    }

    return p_state;
}

/*****************************************************************************
 *
 * Functions related to state transitions.
 *
 *****************************************************************************/

/**
 * @brief Generic handler for entering a HSM state.
 *
 * @note This is an auxiliary function and should not be called by application.
 *
 * @param[in] p_hsm        Pointer to HSM instance.
 * @param[in] p_next_state Pointer to state being entered.
 */
static inline void hsm_state_entry(hsm_t *p_hsm, hsm_state_t *p_next_state)
{
    while (p_next_state != NULL)
    {
        hsm_state_t *p_next_next_state = NULL;

        switch (p_next_state->type)
        {
            case HSM_STATE_TYPE_FULL_LEAF:
            {
                hsm_state_full_leaf_t *p_full_leaf_state =
                                    (hsm_state_full_leaf_t *) p_next_state;

                if (p_full_leaf_state->entry != HSM_NO_ENTRY)
                {
                    p_full_leaf_state->entry();
                }
                break;
            }

            case HSM_STATE_TYPE_PARENT:
            {
                hsm_state_parent_t *p_parent_state =
                                    (hsm_state_parent_t *) p_next_state;

                p_next_next_state = p_parent_state->p_nested_state;
                break;
            }

            case HSM_STATE_TYPE_FULL_PARENT:
            {
                hsm_state_full_parent_t *p_full_parent_state =
                                    (hsm_state_full_parent_t *) p_next_state;

                if (p_full_parent_state->entry != HSM_NO_ENTRY)
                {
                    p_full_parent_state->entry();
                }

                p_next_next_state = p_full_parent_state->p_nested_state;
                break;
            }
            default:
                /** No entry point. */
                break;
        }

        (void) hsm_push_state(p_hsm, p_next_state);
        p_next_state = p_next_next_state;
    } 
}


/**
 * @brief Generic handler for exiting a HSM state.
 *
 * @note This is an auxiliary function and should not be called by application.
 *
 * @param[in] p_hsm           Pointer to HSM instance.
 * @param[in] p_current_state Pointer to state being exited.
 */
static inline void hsm_state_exit(hsm_t *p_hsm, hsm_state_t *p_current_state)
{
    hsm_state_t *p_state;

    do
    {
        p_state = hsm_pop_state(p_hsm);
        
        if (p_state != NULL)
        {
            switch (p_state->type)
            {
                case HSM_STATE_TYPE_FULL_LEAF:
                {
                    hsm_state_full_leaf_t *p_full_leaf_state =
                                        (hsm_state_full_leaf_t *) p_state;
                    if (p_full_leaf_state->exit != HSM_NO_EXIT)
                    {
                        p_full_leaf_state->exit();
                    }
                    break;
                }

                case HSM_STATE_TYPE_FULL_PARENT:
                {
                    hsm_state_full_parent_t *p_full_parent_state =
                                        (hsm_state_full_parent_t *) p_state;
                    if (p_full_parent_state->exit != HSM_NO_EXIT)
                    {
                        p_full_parent_state->exit();
                    }
                    break;
                }
                default:
                    /** No exit point. */
                    break;
            }
        }
    } while (p_current_state != p_state);
}


/**
 * @brief Update state transition history with most recent transition.
 *
 * @param[in] p_hsm           Pointer to HSM instance.
 * @param[in] p_next_state    Pointer to next state.
 */
static inline void hsm_state_history_update(hsm_t *p_hsm, hsm_state_t *p_next_state)
{
    hsm_state_t *p_state = p_hsm->p_hsm_stack[p_hsm->stack_top];

    if ((p_state != NULL) &&
        ((p_state->type == HSM_STATE_TYPE_PARENT) ||
         (p_state->type == HSM_STATE_TYPE_FULL_PARENT)))
    {
        hsm_state_parent_t *p_current_state = (hsm_state_parent_t *) p_state;

        if (p_current_state->history == HSM_HISTORY_SUPPORT)
        {
            p_current_state->p_nested_state = p_next_state;
        }
    }
}

/*****************************************************************************
 *
 * Public HSM functions.
 *
 *****************************************************************************/

void hsm_event_handle(hsm_t *p_hsm, hsm_signal_t signal, void *p_data)
{
    for (EACH_STATE_IN_HSM_NESTED_STATES(p_state, p_hsm))
    {
        p_state->handler(p_hsm, signal, p_data);
    }
}


void hsm_transition_handle(hsm_t            *p_hsm,
                           hsm_transition_t *p_transition,
                           void             *p_data)
{
    if (p_transition->b_is_valid)
    {
        if (!p_transition->b_is_internal)
        {
            hsm_state_exit(p_hsm, p_transition->p_current_state);

            hsm_state_history_update(p_hsm, p_transition->p_next_state);

            if (p_transition->p_action != HSM_NO_ACTION)
            {
                p_transition->p_action(p_data);
            }

            hsm_state_entry(p_hsm, p_transition->p_next_state);
        }
        else if (p_transition->p_action != HSM_NO_ACTION)
        {
            p_transition->p_action(p_data);
        }
    }
}


void hsm_init(hsm_t *p_hsm, hsm_state_t *p_initial_state)
{
    p_hsm->stack_top = 0;
    hsm_state_entry(p_hsm, p_initial_state);
}

