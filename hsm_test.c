/**
 * @file hsm_test.c
 *
 * @brief Simple tests using different features of HSM implementation. A series of
 *        events is fed to a simple HSM and should cause a series of expected 
 *        outputs.
 *
 * Copyright (c) 2017 Henrique Carvalho Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "hsm.h"

bool b_can_transition_to_b;

enum { SIGNAL_A, SIGNAL_B, SIGNAL_1, SIGNAL_2, SIGNAL_F, SIGNAL_G };

void state_a_entry(void);
void state_b_entry(void);
void state_1_entry(void);
void state_2_entry(void);
void state_f_entry(void);
void state_g_entry(void);

void state_a_exit(void);
void state_b_exit(void);
void state_1_exit(void);
void state_2_exit(void);
void state_f_exit(void);
void state_g_exit(void);

void action_on_signal_a(uint8_t *p_data);
void positive_action_on_signal_b(uint8_t *p_data);
void negative_action_on_signal_b(uint8_t *p_data);
void action_on_signal_1(uint8_t *p_data);
void action_on_signal_2(uint8_t *p_data);
void action_on_signal_f(uint8_t *p_data);
void action_on_signal_g(uint8_t *p_data);

HSM_STATE_CREATE_FULL_LEAF(state_f, state_f_entry, state_f_exit);
HSM_STATE_CREATE_FULL_LEAF(state_g, state_g_entry, state_g_exit);
HSM_STATE_CREATE_FULL_LEAF(state_1, state_1_entry, state_1_exit);
HSM_STATE_CREATE_FULL_PARENT(state_2, HSM_HISTORY_SUPPORT, HSM_STATE_REF(state_f), state_2_entry, state_2_exit);
HSM_STATE_CREATE_FULL_LEAF(state_a, state_a_entry, state_a_exit);
HSM_STATE_CREATE_FULL_PARENT(state_b, HSM_HISTORY_SUPPORT, HSM_STATE_REF(state_1), state_b_entry, state_b_exit);

HSM_CREATE(my_hsm, 3);

HSM_STATE_TRANSITION_TABLE(state_a,
{
    HSM_TRANSITION_ON_SIGNAL(SIGNAL_B,
        {
            HSM_GUARD_TRANSITION(b_can_transition_to_b,
                                 HSM_STATE_TRANSITION(HSM_STATE_REF(state_b), positive_action_on_signal_b))
            HSM_GUARD_TRANSITION(!b_can_transition_to_b,
                                 HSM_STATE_TRANSITION(HSM_STATE_REF(state_a), negative_action_on_signal_b))
        });
})

HSM_STATE_TRANSITION_TABLE(state_b,
{
    HSM_TRANSITION_ON_SIGNAL(SIGNAL_A,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(state_a), action_on_signal_a)
        });
})

HSM_STATE_TRANSITION_TABLE(state_1,
{
    HSM_TRANSITION_ON_SIGNAL(SIGNAL_2,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(state_2), action_on_signal_2)
        });
})

HSM_STATE_TRANSITION_TABLE(state_2,
{
    HSM_TRANSITION_ON_SIGNAL(SIGNAL_2,
        {
            HSM_SELF_TRANSITION(action_on_signal_2)
        });
})

HSM_STATE_TRANSITION_TABLE(state_f,
{
    HSM_TRANSITION_ON_SIGNAL(SIGNAL_F,
        {
            HSM_INTERNAL_TRANSITION(action_on_signal_f)
        });
    HSM_TRANSITION_ON_SIGNAL(SIGNAL_G,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(state_g), action_on_signal_g)
        });
})

HSM_STATE_TRANSITION_TABLE(state_g,
{
    HSM_TRANSITION_ON_SIGNAL(SIGNAL_F,
        {
            HSM_STATE_TRANSITION(HSM_STATE_REF(state_f), action_on_signal_f);
        });
})

int main() {
    uint8_t event;
    
    hsm_init(&my_hsm, HSM_STATE_REF(state_a));
    printf("-------\n");
    event = 0;
    b_can_transition_to_b = false;
    hsm_event_handle(&my_hsm, SIGNAL_B, &event);
	printf("-------\n");
    event = 1;
    b_can_transition_to_b = true;
    hsm_event_handle(&my_hsm, SIGNAL_B, &event);
	printf("-------\n");
    event = 2;
    hsm_event_handle(&my_hsm, SIGNAL_2, &event);
	printf("-------\n");
    event = 3;
    hsm_event_handle(&my_hsm, SIGNAL_F, &event);
	printf("-------\n");
    event = 4;
    hsm_event_handle(&my_hsm, SIGNAL_F, &event);
	printf("-------\n");
    event = 5;
    hsm_event_handle(&my_hsm, SIGNAL_F, &event);
	printf("-------\n");
    event = 6;
    hsm_event_handle(&my_hsm, SIGNAL_G, &event);
	printf("-------\n");
    event = 7;
    hsm_event_handle(&my_hsm, SIGNAL_A, &event);
	printf("-------\n");
    event = 8;
    hsm_event_handle(&my_hsm, SIGNAL_B, &event);
	printf("-------\n");
	return 0;
}

void state_a_entry(void)
{
    printf("Entering State A\n");
}

void state_b_entry(void)
{
    printf("Entering State B\n");
}

void state_1_entry(void)
{
    printf("Entering State 1\n");
}

void state_2_entry(void)
{
    printf("Entering State 2\n");
}

void state_f_entry(void)
{
    printf("Entering State F\n");
}

void state_g_entry(void)
{
    printf("Entering State G\n");
}


void state_a_exit(void)
{
    printf("Exiting State A\n");
}

void state_b_exit(void)
{
    printf("Exiting State B\n");
}

void state_1_exit(void)
{
    printf("Exiting State 1\n");
}

void state_2_exit(void)
{
    printf("Exiting State 2\n");
}

void state_f_exit(void)
{
    printf("Exiting State F\n");
}

void state_g_exit(void)
{
    printf("Exiting State G\n");
}


void action_on_signal_a(uint8_t *p_data)
{
    printf("Received Event %d on Signal A\n", *p_data);
}

void positive_action_on_signal_b(uint8_t *p_data)
{
    printf("Received Event %d on Signal B\n", *p_data);
}

void negative_action_on_signal_b(uint8_t *p_data)
{
    printf("Received Event %d on Signal B, but can't do anything about it...\n", *p_data);
}

void action_on_signal_1(uint8_t *p_data)
{
    printf("Received Event %d on Signal 1\n", *p_data);
}

void action_on_signal_2(uint8_t *p_data)
{
    printf("Received Event %d on Signal 2\n", *p_data);
}

void action_on_signal_f(uint8_t *p_data)
{
    printf("Received Event %d on Signal F\n", *p_data);
}

void action_on_signal_g(uint8_t *p_data)
{
    printf("Received Event %d on Signal G\n", *p_data);
}
